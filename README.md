# cars-api

## Introduction

This application serves RESTful API (with a MongoDB backend) to create, update, delete, and get information about 
individual cars. An additional route is provided to query car metadata.

A car has the following attributes:

* brand (string)
* color (string)
* model (string)
* owners (integer)
* horsepower (integer)

Please note that all attributes are mandatory, i.e., a new car cannot 
be created if even a single attribute is missing in the `create` request.

## Setup

The application requires `docker` and `docker-compose`. The Docker container, which contains both the application 
server and the MongoDB server, can be built and deployed using the following simple command:

```docker-compose build && docker-compose up```

The application should then be available on the following host and port (assuming deployment on locla machine):

```http://0.0.0.0:3000/```

The main application file is `server.js`.

## Routes

The following routes are available:

* GET `/`: Returns general information about the application.
* GET `/api/cars`: To get metadata about cars.
* POST `/api/car`: To create a new car.
* GET `/api/car:id`: To retrieve a car with the given `id``
* PUT `/api/car:id`: To update one or more attributes of an existing car corresponding to `id`. 

## Examples

### Get application information

**Request**

```curl http://0.0.0.0:3000/```

**Response**

```
{"name":"cars-api","version":"0.0.1","status":"up"}
```

### Get metadata

**Request**

```
curl http://0.0.0.0:3000/api/cars
```

**Response**

```
{"status":"success","result":{"brand":"String","color":"String","model":"String","owners":"Number","horsepower":"Number"}}
```

### Create a new car

**Request**

```
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"brand":"ford", "color":"black", "model":"focus", "owners": 1, "horsepower":150}' \
  http://0.0.0.0:3000/api/car
```

**Response**

```
{"status":"success","result":{"id":"61a7aaee420a502e78748f5b"}}
```

### Get a specific car

**Request**

```
curl http://0.0.0.0:3000/api/car61a7aaee420a502e78748f5b
```

**Response**

```
{"status":"success","result":{"car":{"brand":"ford","color":"black","model":"focus","owners":1,"horsepower":150}}}
```

### Update an existing car

**Request**

```
curl --header "Content-Type: application/json" \
  --request PUT \
  --data '{"brand":"BMW", "model":"M140i"}' \
  http://0.0.0.0:3000/api/car61a7ae28ca327d68712040d7
```

**Response**

```
{"status":"success","result":{"car":{"brand":"BMW","color":"black","model":"M140i","owners":1,"horsepower":150}}}
```

### Delete a car

**Request**

```
curl --request DELETE  http://0.0.0.0:3000/api/car61a7ae28ca327d68712040d7
```

**Response**

```
{"status":"success","result":{"_id":"61a7ae28ca327d68712040d7","brand":"BMW","color":"black","model":"M140i","owners":1,"horsepower":150,"__v":0}}
```
