/**
 * The application entry point for the RESTful car-api.
 *
 * This module has two responsibilities:
 *  1. Establish the connection with the database and start the
 *      application server.
 *  2. Defines and handles all routes for the RESTful API.
 */

// Node.js internal packages
const path = require('path')

// 3rd party packages
const mongoose = require('mongoose')
const express = require('express')

// Internal packages
const CarController = require('./controllers/car.controller')

// Initialize express and any required middlewares
const app = express()
app.use(express.json())

// Server connection information
const PORT = 3000
const HOST = '0.0.0.0'

// Connect to the database and start the server
mongoose.connect(
    'mongodb://mongo:27017/carsdb',
    { useNewUrlParser: true }).then(() => {
    console.log('Successfully connected to MongoDB.')

    app.listen(PORT, HOST, () => {
        console.log(`Cars app listening on: ${HOST}:${PORT}`)
    })
})

/* Routes */

// Update car
app.put('/api/car:id', (request, response) => {
    CarController.updateCar(request.params.id, request.body).then(result => {
        response.json(result)
    })
})

// Delete car
app.delete('/api/car:id', (request, response) => {
    CarController.deleteCar(request.params.id).then(result => {
        response.json(result)
    })
})

// Get car metadata
app.get('/api/cars', (request, response) => {
    // TODO: The returned object is not what is required ...
    CarController.getMetaData().then(result => {
        response.json(result)
    })
})

// Get specific car
app.get('/api/car:id', (request, response) => {
    CarController.getCar(request.params.id).then(result => {
        response.json(result)
    })
})

// Create car
app.post('/api/car', (request, response) => {
    CarController.createCar(request.body).then(result => {
        response.json(result)
    })
})

// API information
app.get('/', function (req, res) {
    const pkg = require(path.join(__dirname, 'package.json'))
    res.json({
        name: pkg.name,
        version: pkg.version,
        status: 'up'
    })
})

// All invalid routes
app.use(function (req, res, next) {
    res.status(404)
    res.end()
})
