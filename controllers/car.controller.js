/**
 * This module serves as the core application logic engine. It receives
 * input data from all routes, calls the data processing service to
 * interact with the database, and then finally constructs the response
 * to be sent back to the client.
 *
 * @module car/controller
 */

const CarService = require('../services/car.service')
const { validateAll, validateSome } = require('../validators/car.validator')

/**
 * @classdesc This class defines the controller interface for the core
 * application logic processing engine.
 */
class CarController {
    /**
     * Returns the blueprint for a Car in the database, i.e., the
     * attributes that define a car, along with their data types.
     *
     * @returns On success returns a promise that resolves to an
     *      object of the form {
     *          status: <error or success>,
     *          result: <metadata object or {}>
     *      }
     *      In case of error, the 'status' property is set to 'error',
     *      and the 'result' property is set to {}.
     */
    async getMetaData () {
        // Object to be returned to the caller
        let result = {}
        // Flag: If set, indicates if a service error has been encountered
        let isServiceError = false

        // Get metadata from the service
        const metaData = await CarService.getMetaData().catch(e => {
            console.log(`Controller error in getMetaData(): ${e.message}`)

            // Set error flag
            isServiceError = false
        })

        // In case there's an error or no metadata was received,
        if (isServiceError || metaData == null) {
            // There must have been a problem. Therefore, prepare an error
            // response.
            result = this._populateErrorResult()
        } else if (result.status !== 'error') {
            // Prepare the result object with the required metadata
            result.status = 'success'
            result.result = metaData
        }

        return result
    }

    /**
     * Updates one or more attributes of a single car that already exists
     * in the database.
     *
     * @param carId: The unique 'id' used to identify the document. This value
     *          is returned by the {@link createCar} function when the document is
     *          first created.
     * @param data: Object, where each property refers an attribute to be updated,
     *          and the corresponding value is the one to be assigned.
     *
     * @returns On success returns a promise that resolves to an
     *      object of the form {
     *          status: <error or success>,
     *          result: <{car: {properties: values}}> or {}
     *      }
     *      In case of an error, the 'status' property is set to 'error'. The
     *      'result' property is set to {} if it is a service error, and with
     *      an object containing validation error information.
     */
    async updateCar (carId, data) {
        // Object to be returned to the caller
        let result = {}
        // Flag: If set, indicates if a service error has been encountered
        let isServiceError = false

        // Validate input data to make sure if conforms to the schema
        const validationResult = validateSome(data)

        // If validation failed,
        if (validationResult.status === 'error') {
            console.log(`Validation error in updateCar(): ${validationResult.errors}`)

            // Prepare a response object that contains the validation errors
            result = this._populateErrorResult(validationResult.errors)
        } else {
            // Call the update service
            const car = await CarService.updateCar(carId, data).catch(e => {
                console.log(`Controller error in updateCar(): ${e.message}`)

                // Set error flag
                isServiceError = true
            })

            // In case there's an error or no metadata was received,
            if (isServiceError || car == null) {
                // There must have been a problem. Therefore, prepare an error
                // response.
                result = this._populateErrorResult()
            } else if (result.status !== 'error') {
                // Prepare the successful response
                result.status = 'success'
                result.result = {}
                result.result.car = this._populateCarFrom(car)
            }
        }

        return result
    }

    /**
     *  Retrieves all data corresponding to the given 'carId'.
     *
     * @param carId: The unique 'id' used to identify the document. This value
     *          is returned by the {@link createCar} function when the document is
     *          first created.
     * @returns On success returns a promise that resolves to an
     *      object of the form {
     *          status: <error or success>,
     *          result: <{car: {properties: values}}> or {}
     *      }
     *      In case of an error, the 'status' property is set to 'error'. The
     *      'result' property is set to {} if it is a service error.
     */
    async getCar (carId) {
        // Object to be returned to the caller
        let result = {}
        // Flag: If set, indicates if a service error has been encountered
        let isServiceError = false

        // Get car data from the service
        const car = await CarService.getCar(carId).catch(e => {
            console.log(`Controller error: ${e.message}`)

            // Set error flag
            isServiceError = true
        })

        // In case there's an error or no metadata was received,
        if (isServiceError || car == null) {
            // There must have been a problem. Therefore, prepare an error
            // response.
            result = this._populateErrorResult()
        } else if (result.status !== 'error') {
            // Prepare the successful response
            result.status = 'success'
            result.result = {}
            result.result.car = this._populateCarFrom(car)
        }

        return result
    }

    /**
     * Creates a new car using the given data object.
     *
     * @param data: Object containing all the required attributes as
     *          defined in car.model.CarSchema.
     *
     * @returns On success returns a promise that resolves to an
     *      object of the form {
     *          status: <error or success>,
     *          result: {id: <unique value>} or {}
     *      }, which contains the newly created car data.
     *      In case of an error, the 'status' property is set to 'error'. The
     *      'result' property is set to {} if it is a service error.
     */
    async createCar (data) {
        // Object to be returned to the caller
        let result = {}
        // Flag: If set, indicates if a service error has been encountered
        let isServiceError = false

        // Validate input data to make sure if conforms to the schema
        const validationResult = validateAll(data)

        // If validation failed,
        if (validationResult.status === 'error') {
            console.log(`Validation error in createCar(): ${validationResult.errors}`)

            // Prepare a response object that contains the validation errors
            result = this._populateErrorResult(validationResult.errors)
        } else {
            // Create the car
            const car = await CarService.createCar(data).catch(e => {
                console.log(`Controller error in createCar(): ${e.message}`)

                // Set error flag
                isServiceError = true
            })

            // In case there's an error or no metadata was received,
            if (isServiceError || car == null) {
                // There must have been a problem. Therefore, prepare an error
                // response.
                result = this._populateErrorResult()
            } else {
                // Prepare the successful response
                result.status = 'success'
                result.result = { id: car._id.toHexString() }
            }
        }

        return result
    }

    /**
     * Deletes the car corresponding to the given 'carId'.
     *
     * @param carId: The unique 'id' used to identify the document. This value
     *          is returned by the {@link createCar} function when the document is
     *          first created.
     *
     * @returns On success returns a promise that resolves to an
     *      object of the form {
     *          status: <error or success>,
     *          result: <object containing the deleted object> or {}
     *      }
     *      In case of an error, the 'status' property is set to 'error'. The
     *      'result' property is set to {} if it is a service error.
     */
    async deleteCar (carId) {
        // Object to be returned to the caller
        let result = {}
        // Flag: If set, indicates if a service error has been encountered
        let isServiceError = false

        // Delete the car
        const query = await CarService.deleteCar(carId).catch(e => {
            console.log(`Controller error in deleteCar(): ${e.message}`)

            // Set error flag
            isServiceError = true
        })

        // In case there's an error or no metadata was received,
        if (isServiceError || query == null) {
            // There must have been a problem. Therefore, prepare an error
            // response.
            result = this._populateErrorResult()
        } else if (result.status !== 'error') {
            // Prepare the successful response
            result.status = 'success'
            result.result = query
        }

        return result
    }

    /**
     * Copies selected public properties of the given object to create a new
     * object.
     *
     * @param data: A car object from Mongoose.
     *
     * @returns Object with all public car attributes.
     *
     * @private
     */
    _populateCarFrom (data) {
        const car = {}

        // For each car attribute,
        for (const attribute of CarService.getCarAttributes()) {
            // Copy the attribute from the given object
            car[attribute] = data[attribute]
        }

        return car
    }

    /**
     * Prepares an error result object.
     *
     * @param errors: Object error data, which will be included in the 'result'
     *          if provided.
     *
     * @returns {{result: *, status: string}}: Error response.
     *
     * @private
     */
    _populateErrorResult (errors) {
        return {
            status: 'error',
            result: errors || {} // Include error is provided, otherwise use {}
        }
    }
}

/**
 * @exports car/controller
 */
module.exports = new CarController()
