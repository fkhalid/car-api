/**
 * This module defines the interface that allows the application to
 * interact with the database via CRUD operations.
 *
 * Note: Exception handling has been kept deliberately simple. The idea
 * is to prevent the server from crashing, log exceptions on the server
 * side, and delegate preparation of error messages for the client to the
 * application logic layer.
 *
 * @module car/service
 *
 */

// Reference to the database model
const { Car } = require('../models/car.model')

/**
 * @classdesc This class defines the interface for CRUD operations on
 * the database. All member functions are async.
 */
class CarService {
    /**
     * Returns a list of names of all exposed car attributes.
     *
     * @returns {string[]}: Names of all attributes.
     */
    getCarAttributes () {
        return Object.keys(Car.schema.obj)
    }

    /**
     * Returns the blueprint for a Car in the database, i.e., the
     * attributes that define a car, along with their data types.
     *
     * @returns A Promise that resolves to an Object, where each
     * property corresponds to the name of a Car attribute and the
     * corresponding value represents the data type for the attribute.
     */
    async getMetaData () {
        const result = {}

        // For each Car attribute,
        for (const attribute of Object.keys(Car.schema.obj)) {
            // Set the name of the attribute as the key, and the
            // corresponding data type as the value.
            result[attribute] = Car.schema.paths[attribute].instance
        }

        return result
    }

    /**
     * Updates one or more attributes of a single car that already exists
     * in the database.
     *
     * @param carId: The unique 'id' used to identify the document. This value
     *          is returned by the {@link createCar} function when the document is
     *          first created.
     * @param data: Object, where each property refers an attribute to be updated,
     *          and the corresponding value is the one to be assigned.
     * @returns On success, a promise that resolves to an Object, which contains
     *          the updated document. On failure, an empty Object.
     */
    async updateCar (carId, data) {
        let result = {}

        try {
            result = await Car.findByIdAndUpdate(
                carId, data, { new: true })
        } catch (e) {
            console.log(`Service error in updateCar(): ${e.message}`)
        }

        return result
    }

    /**
     * Deletes the document corresponding to the given 'carId'.
     *
     * @param carId: The unique 'id' used to identify the document. This value
     *          is returned by the {@link createCar} function when the document is
     *          first created.
     * @returns On success, a promise that resolves to an Object, which contains
     *          the deleted document. On failure, an empty Object.
     */
    async deleteCar (carId) {
        let result = {}

        try {
            result = await Car.findByIdAndDelete(carId)
        } catch (e) {
            console.log(`Service error in deleteCar(): ${e.message}`)
        }

        return result
    }

    /**
     * Retrieves the document corresponding to the given 'carId'.
     *
     * @param carId: carId: The unique 'id' used to identify the document. This value
     *          is returned by the createCar() function when the document is
     *          first created.
     * @returns On success, a promise that resolves to an Object, which contains
     *          the required document. On failure, an empty Object.
     */
    async getCar (carId) {
        let result = {}

        try {
            result = await Car.findById(carId)
        } catch (e) {
            console.log(`Service error in getCar(): ${e.message}`)
        }

        return result
    }

    /**
     * Creates a new document using the given 'car' data object.
     *
     * @param car: Object containing all the required attributes as
     *          defined in car.model.CarSchema.
     *
     * @returns On success, a promise that resolves to an Object, which contains
     *          the created document. On failure, an empty Object.
     */
    async createCar (car) {
        let result = {}

        try {
            result = await Car.create(car)
        } catch (e) {
            console.log(`Service error in createCar(): ${e.message}`)
        }

        return result
    }
}

/**
 * @exports car/service
 */
module.exports = new CarService()
