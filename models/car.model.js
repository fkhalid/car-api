/**
 * This module represents the 'model' layer of the application
 * architecture, and defines the database schema.
 *
 * @module car/model
 *
 * */

// Use the Mongoose framework to simplify interaction with MongoDB
const mongoose = require('mongoose')

/**
 * MongoDB schema that serves as the blueprint for an individual car.
 *
 * @type {module:mongoose.Schema}
 */
const CarSchema = new mongoose.Schema({
    brand: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    owners: {
        type: Number,
        required: true
    },
    horsepower: {
        type: Number,
        required: true
    }
})

// Use the above defined schema to create a model
const Car = mongoose.model('cars', CarSchema)

/**
 * @exports car/model
 */
module.exports = {
    /**
     * Reference to the car model.
     *
     * @type {module:mongoose.model}
     * */
    Car
}
