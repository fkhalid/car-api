/**
 * This module contains helper functions and data structures required for
 * input data validation.
 *
 * @module car/validator
 */

const Ajv = require('ajv')

const ajv = new Ajv()

/**
 * The schema used for validation. It has been designed such that it can be
 * used both for validating all the required fields for the create()
 * operation, as well as for selected fields for the update() operation.
 *
 * Source: https://github.com/ajv-validator/ajv/issues/211
 */
const schema = {
    $id: 'car',
    type: 'object',
    allOf: [
        {
            $id: '#brand',
            properties: {
                brand: { type: 'string' }
            },
            required: ['brand']
        },
        {
            $id: '#color',
            properties: {
                color: { type: 'string' }
            },
            required: ['color']
        },
        {
            $id: '#model',
            properties: {
                model: { type: 'string' }
            },
            required: ['model']
        },
        {
            $id: '#owners',
            properties: {
                owners: { type: 'integer' }
            },
            required: ['owners']
        },
        {
            $id: '#horsepower',
            properties: {
                horsepower: { type: 'integer' }
            },
            required: ['horsepower']
        }
    ]
}

// Compiled schema definition that can be used as a validation function
// for full schema validation
const conformsToSchema = ajv.compile(schema)

/**
 * Validates input data against the complete schema, i.e, all the
 * required fields must be present.
 *
 * @param data: Input data to validate
 *
 * @returns The result object. if validation is successful, it only
 *          contains the 'status' field set to 'success'. Otherwise,
 *          'status' is set to 'error', and an 'errors' field is
 *          added which contains error data.
 */
function validateAll (data) {
    // Object to be returned
    const result = {
        status: 'success'
    }

    // If validation fails,
    if (!conformsToSchema(data)) {
        // Initialize the result object
        result.status = 'error'
        result.errors = []

        // All all errors to result.errors
        for (const element of conformsToSchema.errors) {
            result.errors.push({
                attribute: element.instancePath,
                error: element.message
            })
        }
    }

    return result
}

/**
 * Validates input data against the schema. However, it does not
 * require the data object to have all the required fields. One
 * or more attributes are enough.
 *
 * Each attribute in the data object is validated individually.
 *
 * @param data: Input data to validate.
 *
 * @returns The result object. if validation is successful, it only
 *          contains the 'status' field set to 'success'. Otherwise,
 *          'status' is set to 'error', and an 'errors' field is
 *          added which contains error data.
 */
function validateSome (data) {
    // Object to be returned
    const result = {
        status: 'success'
    }

    // For each attribute in the input data,
    for (const [key, value] of Object.entries(data)) {
        // Convert the key/value pair into an object
        const objToValidate = {}
        objToValidate[key] = value

        // Validate the attribute against the schema
        const isSchemaValid = ajv.validate(
            { $ref: `car#${key}` },
            objToValidate
        )

        // If validation failed for the current attribute,
        if (!isSchemaValid) {
            // Prepare error result
            result.status = 'error'
            result.errors = `Invalid data type for ${key}`

            break
        }
    }

    return result
}

/**
 * @exports car/validator
 */
module.exports = { validateAll, validateSome }
