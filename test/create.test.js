
const assert = require('assert')
const axios = require('axios')
const { describe, it } = require('mocha')

describe('Successful integration', () => {
    let carId = null

    describe('Get metadata', () => {
        it('should return metadata', async () => {
            const groundTruth = {
                status: 'success',
                result: {
                    brand: 'String',
                    color: 'String',
                    model: 'String',
                    owners: 'Number',
                    horsepower: 'Number'
                }
            }

            const result = await axios.get('http://0.0.0.0:3000/api/cars')

            assert.deepStrictEqual(result.data, groundTruth)
        })
    })

    describe('Successful create', () => {
        it('should create a new car and return car id', async () => {
            const result = await axios.post('http://0.0.0.0:3000/api/car', {
                brand: 'ford',
                color: 'black',
                model: 'focus',
                owners: 1,
                horsepower: 150
            })

            carId = result.data.result.id

            assert.strictEqual(result.data.status, 'success')
            // eslint-disable-next-line no-prototype-builtins
            assert(result.data.result.hasOwnProperty('id'))
        })
    })

    describe('Get car', () => {
        it('should return the requested car data', async () => {
            const groundTruth = {
                status: 'success',
                result: {
                    car: {
                        brand: 'ford',
                        color: 'black',
                        model: 'focus',
                        owners: 1,
                        horsepower: 150
                    }
                }
            }

            const result = await axios.get(`http://localhost:3000/api/car${carId}`)

            assert.deepStrictEqual(result.data, groundTruth)
        })
    })

    describe('Update car', () => {
        it('should return the updated car data', async () => {
            const groundTruth = {
                status: 'success',
                result: {
                    car: {
                        brand: 'BMW',
                        color: 'black',
                        model: 'M140i',
                        owners: 1,
                        horsepower: 150
                    }
                }
            }

            const result = await axios.put(
                `http://localhost:3000/api/car${carId}`,
                {
                    brand: 'BMW',
                    model: 'M140i'
                })

            assert.deepStrictEqual(result.data, groundTruth)
        })
    })

    describe('Delete car', () => {
        it('should return the updated car data', async () => {
            const groundTruth = {
                status: 'success',
                result: {
                    __v: 0,
                    _id: carId,
                    brand: 'BMW',
                    color: 'black',
                    model: 'M140i',
                    owners: 1,
                    horsepower: 150
                }
            }

            const result = await axios.delete(`http://localhost:3000/api/car${carId}`)

            assert.deepStrictEqual(result.data, groundTruth)
        })
    })
})

describe('Errors', () => {
    describe('Create car', () => {
        it('should return a validation error', async () => {
            const groundTruth = {}
            groundTruth.status = 'error'
            groundTruth.result = [{
                attribute: '/brand',
                error: 'must be string'
            }]

            const result = await axios.post('http://0.0.0.0:3000/api/car', {
                brand: 1, // Should have been a string
                color: 'black',
                model: 'focus',
                owners: 1,
                horsepower: 150
            })

            assert.strictEqual(result.data.status, 'error')
            assert.deepStrictEqual(result.data, groundTruth)
        })
    })

    describe('Update car', () => {
        it('should return a validation error', async () => {
            const groundTruth = {
                status: 'error',
                result: 'Invalid data type for brand'
            }

            const result = await axios.put(
                'http://localhost:3000/api/carjujuju',
                {
                    brand: 1, // Supposed to be a string
                    model: 'M140i'
                })

            assert.deepStrictEqual(result.data, groundTruth)
        })
    })
})
